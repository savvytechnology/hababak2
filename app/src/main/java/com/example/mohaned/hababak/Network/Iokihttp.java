package com.example.mohaned.hababak.Network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.widget.Toast;

import com.example.mohaned.hababak.R;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.os.Looper.getMainLooper;

public class Iokihttp {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    public Response uploadImage(File image, String imageName, String url) throws IOException {

        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", imageName, RequestBody.create(MEDIA_TYPE_PNG, image))
                .build();
        Request request = new Request.Builder().url(url)
                .post(requestBody).build();
        Response response = client.newCall(request).execute();
        return response;
    }
    public Call post(final String url, String json, final Callback callback) {
        System.out.println("url is :" + url);
        final RequestBody body = RequestBody.create(JSON, json);
        Handler handler = new Handler(getMainLooper());
        final Call[] call = new Call[1];
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                call[0] = client.newCall(request);
                call[0].enqueue(callback);
            }
        }, 10000);


        return call[0];
    }

    public boolean isNetworkConnected(Context context) {
        if(isConnected(context)){
            return true;}
        else{
            Toast.makeText(context,context.getResources().getString(R.string.check_internet),Toast.LENGTH_SHORT).show();
            return false;
        }

    }
    private boolean isConnected(Context context){

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    public boolean isInternetAvailable(Context context) {
        try {
            InetAddress ipAddr = InetAddress.getByName(context.getResources().getString(R.string.url));
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

}
