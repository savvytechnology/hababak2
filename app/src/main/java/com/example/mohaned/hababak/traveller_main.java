package com.example.mohaned.hababak;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.SparseArray;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohaned.hababak.Adapter.companion_adapter;
import com.example.mohaned.hababak.Counter_phone_key.Country;
import com.example.mohaned.hababak.Counter_phone_key.CountryAdapter;
import com.example.mohaned.hababak.Counter_phone_key.CustomPhoneNumberFormattingTextWatcher;
import com.example.mohaned.hababak.Counter_phone_key.OnPhoneChangedListener;
import com.example.mohaned.hababak.Counter_phone_key.PhoneUtils;
import com.example.mohaned.hababak.DB.AirlinesReaderContract;
import com.example.mohaned.hababak.DB.FlightsReaderContract;
import com.example.mohaned.hababak.Models.companion_info;
import com.example.mohaned.hababak.Network.Iokihttp;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.tooltip.Tooltip;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class traveller_main extends AppCompatActivity implements OnMapReadyCallback,TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener {
    protected GoogleMap mMap;
    protected SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<ArrayList<Country>>();

    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected Spinner mSpinner;
    protected String mLastEnteredPhone;
    protected EditText mPhoneEdit;
    protected CountryAdapter mAdapter;
    private String date;
    private String time;
    private LIFOqueue lifo;
    public DatePickerDialog dpd,dpd2;
    public Calendar now;
    ArrayList<companion_info> dataModels;
    ListView listView;
    private static companion_adapter adapter;
    private View traveller_include,companion_include,payment_include,package_include,success_process_include;
    private boolean main=true;
    private boolean skip_companion=false;
    private Iokihttp okhttp;
    final double[] price = new double[2];
    Intent intent;
    private int sudanese_counter, other_counter, adults, kids;
    private int packageType,paymentMethod,req_type=1;
    private double pickedPrice;
    SharedPreferences shared;
    private String name,passport,nationality,age,airline,flight_no,destination,flight_date,flight_time,country_code,phone,companions_no,luggage_no,medical_note;
    private int req_id;
    List<Object> list;
    View before = null;
    View after = null;
    int companion_no=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traveller_main);
        init();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

    }

    protected OnPhoneChangedListener mOnPhoneChangedListener = new OnPhoneChangedListener() {
        @Override
        public void onPhoneChanged(String phone) {
            try {
                mLastEnteredPhone = phone;
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(phone, null);
                ArrayList<Country> list = mCountriesMap.get(p.getCountryCode());
                Country country = null;
                if (list != null) {
                    if (p.getCountryCode() == 249) {
                        String num = String.valueOf(p.getNationalNumber());
                        if (num.length() >= 3) {
                            String code = num.substring(0, 3);
                        }
                    }
                    if (country == null) {
                        //If no country code is entered, default to US
                        for (Country c : list) {
                            if (c.getCountryCode() == 249) {
                                country = c;
                                break;
                            }
                        }
                    }
                }
                if (country != null) {
                    final int position = country.getNum();
                    mSpinner.post(new Runnable() {
                        @Override
                        public void run() {
                            mSpinner.setSelection(position);
                        }
                    });
                }
            } catch (NumberParseException ignore) {
            }
        }
    };

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date= year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        if(view.getTag()=="Datepickerdialog"){
            TimePickerDialog tpd=TimePickerDialog.newInstance(traveller_main.this,now.HOUR_OF_DAY,now.MINUTE,false);
        tpd.show(getFragmentManager(), "Timepickerdialog");}
        else if(view.getTag()=="Datepickerdialog2"){
            ((TextView)( traveller_include.findViewById(R.id.traveller_age))).setText(date);
        }
    }
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        time = hourOfDay+":"+minute+":"+second;
        ((TextView)traveller_include.findViewById(R.id.datetimeTextView)).setText(date+" "+time);
    }
    public void setList(int count) {
        System.out.println("11");
        View view=findViewById(R.id.companion_include);
        listView=(ListView)view.findViewById(R.id.companion_list);
        System.out.println("22");
        JSONObject jsonObject;

        dataModels= new ArrayList<>();
        if(count==0){}
        else {
            // System.out.println("33"+y);
            for (int x = 0; x < count; x++) {
                companion_info dataModel = new companion_info();
                dataModel.setRow_id(x);
                dataModels.add(dataModel);
            }

            adapter = new companion_adapter(dataModels, getApplicationContext(),getFragmentManager());

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    companion_info dataModel = dataModels.get(position);
                    //   intent.putExtra();
                   // startActivity(intent);
                }
            });
            listView.setAdapter(adapter);
        }
    }

    private void openNextForm() throws JSONException, ParseException {
        System.out.println(lifo.scndsize()+"=SIZE="+lifo.size());
        if (lifo.size()==1){
            findViewById(R.id.main_traveller).setBackground(getResources().getDrawable(R.drawable.lastbg));
            findViewById(R.id.next_linear).setVisibility(View.GONE);
            if(req_type==1) {
                ((TextView) success_process_include.findViewById(R.id.msg)).setText(getString(R.string.please_deliever_departure));
            } else if(req_type==2){
                ((TextView) success_process_include.findViewById(R.id.msg)).setText(getString(R.string.please_deliever_arrival));
            } else if(req_type==3){
                ((TextView) success_process_include.findViewById(R.id.msg)).setText(getString(R.string.please_deliever_medical));
            }
        }

        if(lifo.size()!=0) {
            before = findViewById((Integer) lifo.scndpeek());
            after = null;

            if (before.getId() == R.id.traveler_main_form) {
                if(validateMainForm()){

                    String co_string = ((EditText) before.findViewById(R.id.companion_no)).getText().toString();
                    companion_no=Integer.parseInt(co_string.equals("") ?"0":co_string);

                    if (companion_no==0){
                        skip_companion=true;
                        after = findViewById((Integer) lifo.pop());
                        countPassengers();
                        setPrice(adults, kids, req_type);

                    } else{
                        after = findViewById((Integer) lifo.pop());
                        setList(companion_no);
                        skip_companion=false;
                        animateLayout(before, after);

                    }
                } else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_error),Toast.LENGTH_LONG).show();
                    return;
                }
            } else if (before.getId() == R.id.companion_include) {
                if(validateCompanions(companion_no)){
                    System.out.println("INSIDE PACKAGE");
                    //after =  findViewById((Integer) lifo.pop());
                    countPassengers();
                    setPrice(adults, kids, req_type);
                } else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_error),Toast.LENGTH_LONG).show();
                    return;
                }
            } else if (before.getId() == R.id.service_packages_include) {
                after =  findViewById((Integer) lifo.pop());
                changeTotal();
                sendReq();
            } else if (before.getId() == R.id.payment_methods_include) {
                setPayment();
                if (paymentMethod == 2) {
                    Intent online_payment = new Intent(getApplicationContext(), online_payment.class);
                    shared.edit().putString("price", String.valueOf(pickedPrice)).apply();
                    startActivityForResult(online_payment, 202);
                } else {
                    after = findViewById((Integer) lifo.pop());
                }
            } else{
                after =  findViewById((Integer) lifo.pop());
                animateLayout(before, after);
            }
        } else {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && resultCode == 1) {
            after = findViewById((Integer) lifo.pop());
            animateLayout(before, after);
        } else{
            hideLoading();
        }
    }

    public void animateLayout(final View before, final View after){
        Animator.AnimatorListener animatorListener=new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                System.out.println("SHOW "+after.getId());
                before.setVisibility(View.GONE);
                after.setVisibility(View.VISIBLE);
                //  finalAfter.animate().alpha(1f).setDuration(700);

                main = false;



            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
        before.animate().alpha(0f).setDuration(0).setListener(animatorListener);
        //hideLoading();
    }

    private void setPayment() {
        JSONObject json=new JSONObject();
        JSONObject subJSON=new JSONObject();
        try {
            subJSON.put("id",req_id);
            subJSON.put("req_type",1);
            subJSON.put("payment_method",paymentMethod);
            json.put("method","setPayment");
            json.put("data",subJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(okhttp.isNetworkConnected(getApplicationContext())) {
            showLoading();
            okhttp.post(getString(R.string.lara_url) + "setPayment", json.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("FAIL");
                    hideLoading();

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        try {
                            JSONObject resJSON = new JSONObject(responseStr);
                            //   JSONObject subresJSON = new JSONObject(resJSON.getString("data"));
                            if (Integer.parseInt(resJSON.get("error").toString()) == 1) {
                                if (paymentMethod == 1) {
                                    animateLayout(before, after);
                                }
                                hideLoading();

                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                                    }
                                });
                                onBackPressed();
                                hideLoading();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Response=" + responseStr);

                    } else {
                        System.out.println("Response=" + "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                        hideLoading();
                    }
                }
            });
        }
    }

    private void countPassengers() throws ParseException {
        sudanese_counter = other_counter = adults = kids = 0;

        String co_string=((EditText)traveller_include.findViewById(R.id.companion_no)).getText().toString();
        int companion_no=Integer.parseInt(co_string.equals("") ?"0":co_string);
        int current_year = now.get(Calendar.YEAR);
        String travellerBirthDate = ((TextView) traveller_include.findViewById(R.id.traveller_age)).getText().toString().split("-")[0];
        int traveller_birth_year = Integer.parseInt(travellerBirthDate);
        System.out.println(current_year + "TRAVELLERAGE=" + traveller_birth_year);

        if (current_year - traveller_birth_year > 7) {
            adults++;
        } else {
            kids++;
        }
        for (int x = 0; x < companion_no; x++) {
            String companionBirthDate = ((TextView) companion_include.findViewWithTag("companion" + x).findViewById(R.id.companion_age)).getText().toString().split("-")[0];
            int companion_age = Integer.parseInt(companionBirthDate);
            System.out.println("COMPANION" + x + "AGE=" + companion_age);
            if (current_year - companion_age > 7) {
                adults++;
            } else {
                kids++;
            }
        }
       /* if (((Spinner) findViewById(R.id.nationality)).getSelectedItemId() == 0) {
            sudanese_counter++;
        } else {
            other_counter++;
        }
        for (int x=0;x < companion_no;x++){
            if(((Spinner)(companion_include.findViewWithTag("companion"+x)).findViewById(R.id.companion_nationality)).getSelectedItemId()==0){
                sudanese_counter++;
            }
            else{
                other_counter++;
            }
        }*/
    }

    private void showFields(int i) {
        switch (i) {
            case 1: {
            }
            break;
            case 2: {
                traveller_include.findViewById(R.id.arrival_luggage).setVisibility(View.VISIBLE);
            }
            break;
            case 3: {
                traveller_include.findViewById(R.id.patient_note).setVisibility(View.VISIBLE);
            }
            break;
        }
    }
    private boolean validateMainForm(){
        String phoneArray[]=validate();
        if (phoneArray[0] != null)
            phoneArray[0] = (phoneArray[0].replace("+", "")).trim();
        name=((EditText)traveller_include.findViewById(R.id.traveller_name)).getText().toString();
        passport=((EditText)traveller_include.findViewById(R.id.traveller_passport)).getText().toString();
        nationality = String.valueOf(((Spinner) findViewById(R.id.nationality)).getSelectedItemId() - 1);
        age=((TextView)traveller_include.findViewById(R.id.traveller_age)).getText().toString();
        airline = ((StringWithTag) (((Spinner) traveller_include.findViewById(R.id.traveller_airline)).getSelectedItem())).value;
        flight_no = ((StringWithTag) (((Spinner) traveller_include.findViewById(R.id.traveller_flight_no)).getSelectedItem())).value;
        int airline_position = (((Spinner) traveller_include.findViewById(R.id.traveller_airline)).getSelectedItemPosition());
        int flight_no_position = (((Spinner) traveller_include.findViewById(R.id.traveller_flight_no)).getSelectedItemPosition());

        destination=((EditText)traveller_include.findViewById(R.id.traveller_destination)).getText().toString();
        flight_date=date;
        flight_time=time;
        country_code=phoneArray[0];
        phone=phoneArray[1];
        companions_no=((EditText)traveller_include.findViewById(R.id.companion_no)).getText().toString();
        luggage_no=((EditText)traveller_include.findViewById(R.id.arrival_luggage_no)).getText().toString();
        medical_note=((EditText)traveller_include.findViewById(R.id.medical_note)).getText().toString();

        if (name.isEmpty() || passport.isEmpty() || nationality.isEmpty() || age.isEmpty() || airline_position == 0 || flight_no_position == 0 || destination.isEmpty() || flight_date.isEmpty() || flight_time.isEmpty()) {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_error),Toast.LENGTH_LONG).show();
            return false;
        } else if (req_type == 2 && luggage_no.equals("")) {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_error),Toast.LENGTH_LONG).show();
            return false;
        }
        else {
            return true;
        }
    }

    private void setPrice(int sudanese_no, int other_no, int service_type) {
        JSONObject json=new JSONObject();
        JSONObject subJSON=new JSONObject();
        System.out.println("adults=" + adults + "kids=" + kids);
        try {
            subJSON.put("type",service_type);
            subJSON.put("adults", sudanese_no);
            subJSON.put("kids", other_no);
            json.put("method","getPrice");
            json.put("data",subJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(okhttp.isNetworkConnected(getApplicationContext())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    findViewById(R.id.linear_main).setVisibility(View.GONE);
                    showLoading();
                }
            });
            okhttp.post(getString(R.string.lara_url) + "getPrice", json.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("FAIL");
                    hideLoading();
                    onBackPressed();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                            if (response.isSuccessful()) {
                                String responseStr = null;
                                try {
                                    responseStr = response.body().string();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    JSONObject resJSON = new JSONObject(responseStr);
                                    JSONObject subresJSON = new JSONObject(resJSON.getString("data"));
                                    if (Integer.parseInt(resJSON.get("error").toString()) == 1) {
                                        price[0] = subresJSON.getDouble("classic");
                                        price[1] = subresJSON.getDouble("vip");
                                        ((TextView) package_include.findViewById(R.id.classic_price)).setText(getString(R.string.priceString) + " " + price[0] + " " + getString(R.string.SDG));
                                        ((TextView) package_include.findViewById(R.id.vip_price)).setText(getString(R.string.priceString) + " " + price[1] + " " + getString(R.string.SDG));
                                        after = findViewById((Integer) lifo.pop());
                                        animateLayout(before, after);
                                        hideLoading();
                                    } else {
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        onBackPressed();
                                        hideLoading();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("Response=" + responseStr);

                            } else {
                                System.out.println("Response=" + "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                                hideLoading();
                                onBackPressed();
                            }
                        }
            });
        }
    }

    public void init() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        shared = this.getSharedPreferences("com.example.mohaned.hababak", Context.MODE_PRIVATE);

        okhttp = new Iokihttp();
        traveller_include = findViewById(R.id.traveler_main_form);
        companion_include = findViewById(R.id.companion_include);
        payment_include = findViewById(R.id.payment_methods_include);
        package_include = findViewById(R.id.service_packages_include);
        success_process_include = findViewById(R.id.success_process_include);
        initCodes(getApplicationContext());
        initUI();

        intent = getIntent();
        req_type = intent.getIntExtra("type", 1);
        changeMsg(req_type);
        showFields(req_type);

        Spinner nationality = (Spinner) traveller_include.findViewById(R.id.nationality);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.nationality_list, R.layout.simple_spinner_item_custom);
        // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nationality.setAdapter(adapter);
        now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                traveller_main.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd2 = DatePickerDialog.newInstance(
                traveller_main.this,
                now.get(Calendar.DATE),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        traveller_include.findViewById(R.id.traveller_age).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd2.show(getFragmentManager(), "Datepickerdialog2");
            }
        });
        traveller_include.findViewById(R.id.traveller_age_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd2.show(getFragmentManager(), "Datepickerdialog2");
            }
        });
        traveller_include.findViewById(R.id.datetimeTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        traveller_include.findViewById(R.id.flight_date_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        findViewById(R.id.next_form).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    openNextForm();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        findViewById(R.id.back_form).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        package_include.findViewById(R.id.classic_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePackageLinearBg(view);
            }
        });
        package_include.findViewById(R.id.vip_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePackageLinearBg(view);
            }
        });

        payment_include.findViewById(R.id.cash_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePriceLinearBg(view);
            }
        });
        payment_include.findViewById(R.id.bank_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePriceLinearBg(view);
            }
        });

        success_process_include.findViewById(R.id.done_req).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                //Intent intent=new Intent()
            }
        });


        //setList(4);
        list = new ArrayList<Object>();
        list.add(R.id.traveler_main_form);
        list.add(R.id.companion_include);
        list.add(R.id.service_packages_include);
        list.add(R.id.payment_methods_include);
        list.add(R.id.map_include);
        list.add(R.id.success_process_include);


        lifo = new LIFOqueue(list);
        lifo.pop();
        getAirlines();
        setTooltip();
    }

    public void getFlightNumbers(int airline_id) {
        FlightsReaderContract.FeedReaderDbHelper dbHelper = new FlightsReaderContract.FeedReaderDbHelper(getApplicationContext());

        SQLiteDatabase db = dbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                FlightsReaderContract.FeedEntry.GId,
                FlightsReaderContract.FeedEntry.name,
        };


// How you want the results sorted in the resulting Cursor
        String sortOrder =
                FlightsReaderContract.FeedEntry.name + " DESC";
        String selection = FlightsReaderContract.FeedEntry.airline_id + " = ? ";
        String[] selectionArgs = {String.valueOf(airline_id)};

        Cursor cursor = db.query(
                FlightsReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        ArrayList<StringWithTag> flights = new ArrayList<>();
        flights.add(new StringWithTag(getResources().getString(R.string.flight_number), "0"));
        while (cursor.moveToNext()) {
            int id = cursor.getInt(
                    cursor.getColumnIndexOrThrow(FlightsReaderContract.FeedEntry.GId));
            String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(FlightsReaderContract.FeedEntry.name));
            flights.add(new StringWithTag(name, String.valueOf(id)));
        }
        cursor.close();
        fillFlightNumbers(flights);
    }

    public void getAirlines() {
        AirlinesReaderContract.FeedReaderDbHelper dbHelper = new AirlinesReaderContract.FeedReaderDbHelper(getApplicationContext());

        SQLiteDatabase db = dbHelper.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                AirlinesReaderContract.FeedEntry.GId,
                AirlinesReaderContract.FeedEntry.name
        };


// How you want the results sorted in the resulting Cursor
        String sortOrder =
                AirlinesReaderContract.FeedEntry.name + " DESC";

        Cursor cursor = db.query(
                AirlinesReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        ArrayList<StringWithTag> airlines = new ArrayList<>();
        airlines.add(new StringWithTag(getResources().getString(R.string.airline), "0"));
        while (cursor.moveToNext()) {
            int id = cursor.getInt(
                    cursor.getColumnIndexOrThrow(AirlinesReaderContract.FeedEntry.GId));
            String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(AirlinesReaderContract.FeedEntry.name));
            airlines.add(new StringWithTag(name, String.valueOf(id)));
        }
        cursor.close();
        fillAirlines(airlines);
    }

    public void fillAirlines(final ArrayList airlines) {

        final Spinner airline = (Spinner) traveller_include.findViewById(R.id.traveller_airline);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item_custom, airlines);
        // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        airline.setAdapter(adapter);
        airline.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("@@" + Integer.valueOf(((StringWithTag) (airlines.get(i))).key) + "ff");
                if (Integer.valueOf(((StringWithTag) (airlines.get(i))).key) != 0) {
                    getFlightNumbers(Integer.valueOf(((StringWithTag) (airlines.get(i))).key));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void fillFlightNumbers(final ArrayList flightNumbers) {
        final Spinner flights = (Spinner) traveller_include.findViewById(R.id.traveller_flight_no);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item_custom, flightNumbers);
        flights.setAdapter(adapter);
    }

    private void changeTotal() {
        ((TextView) payment_include.findViewById(R.id.total)).setText(pickedPrice + getResources().getString(R.string.SDG));
    }

    private synchronized void sendReq() throws JSONException {
        JSONArray companionArray = getCompanions(adults + kids - 1);
        final double[] price = new double[2];
        JSONObject json=new JSONObject();
        JSONObject subJSON=new JSONObject();
        try {
            subJSON.put("device",1);
            subJSON.put("type",req_type);
            subJSON.put("login_type",1);
            subJSON.put("customers_id", shared.getString("user_id", ""));
            subJSON.put("name",name);
            subJSON.put("passport",passport);
            subJSON.put("nationality",nationality);
            subJSON.put("age",age);
            subJSON.put("airline",airline);
            subJSON.put("flight_no",flight_no);
            subJSON.put("flight_date",flight_date);
            subJSON.put("flight_time",flight_time);
            subJSON.put("country_code",country_code);
            subJSON.put("phone",phone);
            subJSON.put("companions_no",companions_no);
            subJSON.put("destination",destination);
            subJSON.put("price",pickedPrice);
            System.out.println("Package Type" + packageType);
            subJSON.put("package",packageType);
            subJSON.put("payment_method",paymentMethod);
            subJSON.put("companions",companionArray);
            System.out.println("COMPANIONS " + companionArray.toString());
            subJSON.put("luggages_no", luggage_no);
            subJSON.put("treatment_details", medical_note);
            json.put("method","saveRequest");
            json.put("data",subJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(okhttp.isNetworkConnected(getApplicationContext())) {
            showLoading();
            okhttp.post(getString(R.string.url), json.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("FAIL");
                    hideLoading();
                }
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        try {
                            JSONObject resJSON = new JSONObject(responseStr);
                            JSONObject subresJSON = new JSONObject(resJSON.getString("data"));
                            if (Integer.parseInt(resJSON.get("error").toString()) == 1) {
                                req_id = subresJSON.getInt("id");
                                animateLayout(before, after);
                                hideLoading();
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                        hideLoading();
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Response=" + responseStr);

                    } else {
                        System.out.println("Response=" + response + "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                        hideLoading();

                    }
                }
            });
        }
    }

    public void onBackPressed() {
        if (main) {
            finish();
        } else {
            findViewById(R.id.next_linear).setVisibility(View.VISIBLE);
            View after = findViewById((Integer) lifo.scndpop());
            View before = findViewById((Integer) lifo.scndpeek());
            if (before.getId() == R.id.success_process_include) {
                findViewById(R.id.main_traveller).setBackgroundColor(getResources().getColor(R.color.bgwhite));
            }
            if (after.getId() == R.id.service_packages_include && skip_companion) {
                after.setVisibility(View.GONE);
                after = findViewById((Integer) lifo.scndpop());
                before = findViewById((Integer) lifo.scndpeek());
            }

            final View finalBefore = before;
            final View finalAfter = after;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finalBefore.setVisibility(View.VISIBLE);
                    finalAfter.setVisibility(View.GONE);
                }
            });


            before.setAlpha(1f);
        }
        if (lifo.scndsize() == 1) {
            //Button back = findViewById(R.id.back_form);
            // back.setVisibility(View.GONE);
            main = true;
        }
    }

    public void changePriceLinearBg(View view){
        CardView p1=payment_include.findViewById(R.id.cashC);
        CardView p2=payment_include.findViewById(R.id.bankC);
        CheckBox bankC = payment_include.findViewById(R.id.bank_check);
        CheckBox cashC = payment_include.findViewById(R.id.cash_check);
        TextView selected = (TextView) payment_include.findViewById(R.id.payment_selected);


        if (view.getId() == bankC.getId()) {
            selected.setText(getResources().getString(R.string.online_bank));
            bankC.setChecked(true);
            cashC.setChecked(false);
            bankC.setBackgroundColor(getResources().getColor(R.color.success_green));
            cashC.setBackgroundColor(getResources().getColor(R.color.white));
            paymentMethod=2;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                p1.setElevation(13f);
                p2.setElevation(0f);
            }
        } else if (view.getId() == cashC.getId()) {
            selected.setText(getResources().getString(R.string.cash));
            cashC.setChecked(true);
            bankC.setChecked(false);
            cashC.setBackgroundColor(getResources().getColor(R.color.success_green));
            bankC.setBackgroundColor(getResources().getColor(R.color.trans60));
            paymentMethod=1;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                p1.setElevation(0f);
                p2.setElevation(13f);
            }

        }
    }

    private void setTooltip() {
        final Tooltip.Builder[] builder = new Tooltip.Builder[1];
        package_include.findViewById(R.id.vip_feature).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder[0] = new Tooltip.Builder(view, R.style.Tooltip2)
                        .setCancelable(true)
                        .setDismissOnClick(false)
                        .setCornerRadius(20f)
                        .setText(getResources().getString(R.string.vip_features));
                builder[0].show();
            }
        });
        package_include.findViewById(R.id.classic_feature).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder[0] = new Tooltip.Builder(view, R.style.Tooltip2)
                        .setCancelable(true)
                        .setDismissOnClick(false)
                        .setCornerRadius(20f)
                        .setText(getResources().getString(R.string.classic_features));
                builder[0].show();
            }
        });
    }

    public void changePackageLinearBg(View view) {
        View p1 = package_include.findViewById(R.id.package1);
        View p2 = package_include.findViewById(R.id.package2);
        CheckBox vipC = package_include.findViewById(R.id.vip_check);
        CheckBox classicC = package_include.findViewById(R.id.classic_check);
        TextView selected = (TextView) package_include.findViewById(R.id.package_selected);

        if (view.getId() == classicC.getId()) {
            selected.setText(getResources().getString(R.string.classic));
            vipC.setChecked(false);
            classicC.setChecked(true);
            classicC.setBackgroundColor(getResources().getColor(R.color.success_green));
            vipC.setBackgroundColor(getResources().getColor(R.color.white));
            packageType = 1;
            pickedPrice = price[0];
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                p1.setElevation(3f);
                p2.setElevation(0f);
            }

        } else if (view.getId() == vipC.getId()) {
            selected.setText(getResources().getString(R.string.vip));
            vipC.setChecked(true);
            classicC.setChecked(false);
            vipC.setBackgroundColor(getResources().getColor(R.color.success_green));
            classicC.setBackgroundColor(getResources().getColor(R.color.white));
            packageType = 2;
            pickedPrice = price[1];
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                p1.setElevation(0f);
                p2.setElevation(3f);
            }

        }
        System.out.println(pickedPrice + "__________________________________");
    }
    protected void initCodes(Context context) {
        new traveller_main.AsyncPhoneInitTask(context).execute();
    }
    private JSONArray getCompanions(int companionCount) throws JSONException {
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject;
        String companion_name,companion_passport,companion_nationality,companion_age;
        for(int x=0;x<companionCount;x++){
            companion_name=((EditText)(companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_name))).getText().toString();
            companion_passport=((EditText)(companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_passport))).getText().toString();
            companion_nationality=String.valueOf(((Spinner) companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_nationality)).getSelectedItemId());
            companion_age=((TextView)(companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_age))).getText().toString();
            jsonObject =new JSONObject();
            jsonObject.put("name",companion_name);
            jsonObject.put("passport",companion_passport);
            jsonObject.put("age",companion_age);
            jsonObject.put("nationality",companion_nationality);
            jsonArray.put(x,jsonObject);
        }
        System.out.println("Companion_json="+jsonArray);
        return jsonArray;
    }
    private Boolean validateCompanions(int companionCount) throws JSONException {
        Boolean valid=true;
        System.out.println("NAAAAME"+companionCount);
        String companion_name,companion_passport,companion_nationality,companion_age;
        for(int x=0;x<companionCount;x++){
            companion_name=((EditText)(companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_name))).getText().toString();
            companion_passport=((EditText)(companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_passport))).getText().toString();
            companion_nationality=String.valueOf(((Spinner) companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_nationality)).getSelectedItemId());
            companion_age=((TextView)(companion_include.findViewWithTag("companion"+x).findViewById(R.id.companion_age))).getText().toString();
            System.out.println("NAAAAME"+companion_name);
           if (companion_name.isEmpty() || companion_passport.isEmpty() || companion_nationality.isEmpty() || companion_age.isEmpty()){
               valid=false;
               break;
           }
        }

        return valid;
    }
    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Country c = (Country) mSpinner.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(c.getCountryCodeStr())) {
                return;
            }
            mPhoneEdit.getText().clear();
            mPhoneEdit.getText().insert(mPhoneEdit.getText().length() > 0 ? 1 : 0, String.valueOf(c.getCountryCode()));
            mPhoneEdit.setSelection(mPhoneEdit.length());
            mLastEnteredPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private static class StringWithTag {
        public String key;
        public String value;

        public StringWithTag(String value, String key) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    protected void initUI() {
        mSpinner = (Spinner) traveller_include.findViewById(R.id.spinner);
        mSpinner.setOnItemSelectedListener(mOnItemSelectedListener);

        mAdapter = new CountryAdapter(this);

        mSpinner.setAdapter(mAdapter);

        mPhoneEdit = (EditText) traveller_include.findViewById(R.id.phone);
        mPhoneEdit.addTextChangedListener(new CustomPhoneNumberFormattingTextWatcher(mOnPhoneChangedListener));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (dstart > 0 && !Character.isDigit(c)) {
                        return "";
                    }
                }
                return null;
            }
        };

        mPhoneEdit.setFilters(new InputFilter[]{filter});


        mPhoneEdit.setImeOptions(EditorInfo.IME_ACTION_SEND);
        //mPhoneEdit.setImeActionLabel(getString(R.string.label_send), EditorInfo.IME_ACTION_SEND);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney;

        // Add a marker in Sydney and move the camera
        sydney = new LatLng(15.6181252, 32.5038003);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10));
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    Country c = new Country(mContext, line, i);
                    data.add(c);
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<Country>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<Country> list = mCountriesMap.get(code);
            if (list != null) {
                for (Country c : list) {
                    if (c.getPriority() == 0 || c.getPriority() == 1) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            mAdapter.addAll(data);
            if (mSpinnerPosition > 0) {
                mSpinner.setSelection(mSpinnerPosition);
            }
        }
    }


    protected String[] validate() {
        String region = null;
        String phone = "";
        String keys="";
        String s[]=new String[2];
            try {
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
                StringBuilder sb = new StringBuilder(16);
                StringBuilder key = new StringBuilder(16);
                key.append('+').append(p.getCountryCode());
                sb.append(p.getNationalNumber());
                keys=key.toString();
                phone = sb.toString();
                region = mPhoneNumberUtil.getRegionCodeForNumber(p);
                s[1]=phone;
                s[0]=keys;
                return s;
            } catch (NumberParseException ignore) {
                return s;
            }
    }

    protected void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    protected void showKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
    private void changeMsg(int type) {
        TextView msg = success_process_include.findViewById(R.id.msg);

        switch (type) {
            case 1: {
                msg.setText(getResources().getString(R.string.departure_msg));
            }
            break;
            case 2: {
                msg.setText(getResources().getString(R.string.arrival_msg));
            }
            break;
            case 3: {
                msg.setText(getResources().getString(R.string.medical_msg));
            }break;
        }
    }
    public void showLoading(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                System.out.println("inside run");
                findViewById(R.id.linear_main).setVisibility(View.GONE);
                findViewById(R.id.main_loading).setVisibility(View.VISIBLE);
                System.out.println("after inside run");

            }

        });
        System.out.println("after run");


    }
    public void hideLoading(){
        System.out.println("inside hide");

        final View main=findViewById(R.id.linear_main);
        final ProgressBar loading=findViewById(R.id.main_loading);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                main.setClickable(true);
                main.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    main.setForeground(getDrawable(android.R.color.transparent));
                }
                loading.setVisibility(View.GONE);
            }
        });
    }
}
