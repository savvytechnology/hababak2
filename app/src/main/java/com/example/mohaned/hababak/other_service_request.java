package com.example.mohaned.hababak;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.SparseArray;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohaned.hababak.Counter_phone_key.Country;
import com.example.mohaned.hababak.Counter_phone_key.CountryAdapter;
import com.example.mohaned.hababak.Counter_phone_key.CustomPhoneNumberFormattingTextWatcher;
import com.example.mohaned.hababak.Counter_phone_key.OnPhoneChangedListener;
import com.example.mohaned.hababak.Counter_phone_key.PhoneUtils;
import com.example.mohaned.hababak.Network.Iokihttp;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class other_service_request extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener,DatePickerDialog.OnDateSetListener{
    protected SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<ArrayList<Country>>();
    String pickedPrice;
    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected Spinner mSpinner;
    protected String mLastEnteredPhone;
    protected EditText mPhoneEdit;
    protected CountryAdapter mAdapter;
    LIFOqueue lifo;
    ArrayList<Object> list;
    boolean main=true;
    int req_type;
    View other_req_form, payment_include, success_inlcude;
    ArrayList <String> passport_image,visa_image=null;
    String dateS,timeS,req_id;
    public DatePickerDialog dpd;
    public Calendar now;
    int req_code;
    private Iokihttp okhttp;
    private int paymentMethod=1;
    String traveller_name,traveller_passport,traveller_destination,traveller_visa_type,traveller_have_natCard,traveller_job,flight_date,flight_time,country_code,phone;
    SharedPreferences shared;
    protected OnPhoneChangedListener mOnPhoneChangedListener = new OnPhoneChangedListener() {
        @Override
        public void onPhoneChanged(String phone) {
            try {
                mLastEnteredPhone = phone;
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(phone, null);
                ArrayList<Country> list = mCountriesMap.get(p.getCountryCode());
                Country country = null;
                if (list != null) {
                    if (p.getCountryCode() == 249) {
                        String num = String.valueOf(p.getNationalNumber());
                        if (num.length() >= 3) {
                            String code = num.substring(0, 3);

                        }
                    }
                    if (country == null) {
                        //If no country code is entered, default to US
                        for (Country c : list) {
                            if (c.getCountryCode() == 249) {
                                country = c;
                                break;
                            }
                        }
                    }
                }
                if (country != null) {
                    final int position = country.getNum();
                    mSpinner.post(new Runnable() {
                        @Override
                        public void run() {
                            mSpinner.setSelection(position);
                        }
                    });
                }
            } catch (NumberParseException ignore) {
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_service_request);

        okhttp=new Iokihttp();

        shared = this.getSharedPreferences("com.example.mohaned.hababak", Context.MODE_PRIVATE);

        req_type=getIntent().getIntExtra("type",2);
        other_req_form=findViewById(R.id.other_form_include);
        // passport_include=findViewById(R.id.passport_image_include);
        //visa_include=findViewById(R.id.visa_image_include);
        payment_include=findViewById(R.id.payment_include);
        success_inlcude=findViewById(R.id.success_process_include);

        initCodes(getApplicationContext());
        initUI();

        now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                other_service_request.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        other_req_form.findViewById(R.id.datetimeTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        other_req_form.findViewById(R.id.flight_date_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });
        payment_include.findViewById(R.id.cash_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePriceLinearBg(view);
            }
        });
        payment_include.findViewById(R.id.bank_linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePriceLinearBg(view);
            }
        });

        showFields(req_type);

        findViewById(R.id.next_form).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    openNextActivity();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        findViewById(R.id.back_form).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
        other_req_form.findViewById(R.id.passport_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_code=101;
                openCam(req_code);
            }
        });
        other_req_form.findViewById(R.id.passport_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_code=101;
                openCam(req_code);
            }
        });

        other_req_form.findViewById(R.id.visa_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_code=102;
                openCam(req_code);
            }
        });
        other_req_form.findViewById(R.id.visa_image_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req_code=102;
                openCam(req_code);
            }
        });
        success_inlcude.findViewById(R.id.done_req).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void openCam(int code) {
        Pix.start(other_service_request.this, code, 1);
    }

    public void changePriceLinearBg(View view){
        CardView p1 = payment_include.findViewById(R.id.cashC);
        CardView p2 = payment_include.findViewById(R.id.bankC);
        CheckBox bankC = payment_include.findViewById(R.id.bank_check);
        CheckBox cashC = payment_include.findViewById(R.id.cash_check);
        TextView selected = (TextView) payment_include.findViewById(R.id.payment_selected);


        if (view.getId() == bankC.getId()) {
            selected.setText(getResources().getString(R.string.online_bank));
            bankC.setChecked(true);
            cashC.setChecked(false);
            bankC.setBackgroundColor(getResources().getColor(R.color.success_green));
            cashC.setBackgroundColor(getResources().getColor(R.color.white));
            paymentMethod=2;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                p1.setElevation(13f);
                p2.setElevation(0f);
            }
        } else if (view.getId() == cashC.getId()) {
            selected.setText(getResources().getString(R.string.cash));
            cashC.setChecked(true);
            bankC.setChecked(false);
            cashC.setBackgroundColor(getResources().getColor(R.color.success_green));
            bankC.setBackgroundColor(getResources().getColor(R.color.trans60));
            paymentMethod=1;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                p1.setElevation(0f);
                p2.setElevation(13f);
            }

        }
    }

    private void uploadImage(int code, String image, String name) throws IOException {
        File f = new File(getApplicationContext().getCacheDir(), name);
        f.createNewFile();
        OutputStream os = new BufferedOutputStream(new FileOutputStream(f));

        File imageString = new File(image);
        Bitmap bitmap = new BitmapDrawable(getApplicationContext().getResources(), imageString.getAbsolutePath()).getBitmap();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, os);
        os.close();

        okhttp.uploadImage(f, name, getResources().getString(R.string.url));
        /*File f = new File(image);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeFile(f.getPath());
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] imageBytes = baos.toByteArray();
        System.out.println(imageBytes.length+"^^^^^^^^^^^^^^^^^^^^^^^^^^");
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        JSONObject json=new JSONObject();
        JSONObject subJSON=new JSONObject();
        try {
            //add subjson
            subJSON.put("img",imageString);
            subJSON.put("request_id",req_id);
            json.put("method",code==101?"uploadPassport":"uploadPermit");
            System.out.println("MMMMMMMMMMMMMMMM"+(code==101?"uploadPassport":"uploadPermit"));
            json.put("data",subJSON);
            System.out.println(imageString.length());
            // System.out.println("JSON:"+json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(okhttp.isNetworkConnected(getApplicationContext())) {
            okhttp.post(getString(R.string.url), json.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    System.out.println("FAIL");
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                        }
                    });
                    hideLoading();

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    hideLoading();
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        try {
                            JSONObject resJSON = new JSONObject(responseStr);
//                        JSONObject subresJSON=new JSONObject(resJSON.getString("data"));
                            if (Integer.parseInt(resJSON.get("error").toString()) == 1) {
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Response=" + responseStr);

                    } else {
                        System.out.println("Response=" + "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                    }
                }

            });
        }*/
    }
    private void changeImage(String imageString,View view,int id){
        File f = new File(imageString);
        Bitmap d = new BitmapDrawable(getApplicationContext().getResources(), f.getAbsolutePath()).getBitmap();
        //Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, com.fxn.utility.Utility.getExifCorrectedBitmap(f));
        Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, d);
        ((ImageView)view.findViewById(id)).setImageBitmap(scaled);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(other_service_request.this, req_code,1);
                } else {
                    Toast.makeText(other_service_request.this, "Approve permissions to open ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        dateS= year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        TimePickerDialog tpd=TimePickerDialog.newInstance(other_service_request.this,now.HOUR_OF_DAY,now.MINUTE,false);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        timeS = hourOfDay+":"+minute+":"+second;
        ((TextView)other_req_form.findViewById(R.id.datetimeTextView)).setText(dateS+" "+timeS);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == other_service_request.RESULT_OK && requestCode == 101) {
            passport_image = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            changeImage(passport_image.get(0), other_req_form, R.id.passport_image_view);
        } else if (resultCode == other_service_request.RESULT_OK && requestCode == 102) {
            visa_image = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            changeImage(visa_image.get(0), other_req_form, R.id.visa_image_view);
        }
    }

    protected String[] validate() {
        String region = null;
        String phone = "";
        String keys = "";
        String s[] = new String[2];
        try {
            Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
            StringBuilder sb = new StringBuilder(16);
            StringBuilder key = new StringBuilder(16);
            key.append('+').append(p.getCountryCode());
            sb.append(p.getNationalNumber());
            keys = key.toString();
            phone = sb.toString();
            region = mPhoneNumberUtil.getRegionCodeForNumber(p);
            s[1] = phone;
            s[0] = keys;
            return s;
        } catch (NumberParseException ignore) {
            return s;
        }
    }

    protected void initCodes(Context context) {
        new other_service_request.AsyncPhoneInitTask(context).execute();
    }

    private void showFields(int type){
        list=new ArrayList<Object>();
        switch (type){
            case 1:{
                findViewById(R.id.visitor_linear).setVisibility(View.VISIBLE);
                findViewById(R.id.national_linear).setVisibility(View.VISIBLE);
            }break;
            case 2:
            case 3: {
            }break;
        }
        if (type==2){
            list.add(R.id.other_form_include);
            list.add(R.id.payment_include);
            list.add(R.id.success_process_include);
        }
        else if(type==1 || type==3){
            list.add(R.id.other_form_include);
            //list.add(R.id.passport_image_include);
            // list.add(R.id.visa_image_include);
            list.add(R.id.payment_include);
            list.add(R.id.success_process_include);
        }
        lifo=new LIFOqueue(list);
        lifo.pop();
    }
    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Country c = (Country) mSpinner.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(c.getCountryCodeStr())) {
                return;
            }
            mPhoneEdit.getText().clear();
            mPhoneEdit.getText().insert(mPhoneEdit.getText().length() > 0 ? 1 : 0, String.valueOf(c.getCountryCode()));
            mPhoneEdit.setSelection(mPhoneEdit.length());
            mLastEnteredPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    protected void initUI() {
        mSpinner = (Spinner) other_req_form.findViewById(R.id.spinner);
        mSpinner.setOnItemSelectedListener(mOnItemSelectedListener);

        mAdapter = new CountryAdapter(this);

        mSpinner.setAdapter(mAdapter);

        mPhoneEdit = (EditText) other_req_form.findViewById(R.id.phone);
        mPhoneEdit.addTextChangedListener(new CustomPhoneNumberFormattingTextWatcher(mOnPhoneChangedListener));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (dstart > 0 && !Character.isDigit(c)) {
                        return "";
                    }
                }
                return null;
            }
        };

        mPhoneEdit.setFilters(new InputFilter[]{filter});


        mPhoneEdit.setImeOptions(EditorInfo.IME_ACTION_SEND);
        //mPhoneEdit.setImeActionLabel(getString(R.string.label_send), EditorInfo.IME_ACTION_SEND);


    }
    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<Country>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    Country c = new Country(mContext, line, i);
                    data.add(c);
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<Country>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<Country> list = mCountriesMap.get(code);
            if (list != null) {
                for (Country c : list) {
                    if (c.getPriority() == 0 || c.getPriority() == 1) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            mAdapter.addAll(data);
            if (mSpinnerPosition > 0) {
                mSpinner.setSelection(mSpinnerPosition);
            }
        }
    }
    private boolean validateForm(){
        String phoneArray[]=validate();
        if (phoneArray[0] != null)
            phoneArray[0] = (phoneArray[0].replace("+", "")).trim();
        traveller_name=((EditText)(other_req_form.findViewById(R.id.traveller_name))).getText().toString();
        traveller_passport=((EditText)(other_req_form.findViewById(R.id.traveller_passport))).getText().toString();
        traveller_destination=((EditText)(other_req_form.findViewById(R.id.traveller_destination))).getText().toString();
        int selected_visa_id=((RadioGroup)(other_req_form.findViewById(R.id.visa_group))).getCheckedRadioButtonId();

        traveller_visa_type=(selected_visa_id==R.id.visitor?"1":"2");

        traveller_have_natCard=((CheckBox)(other_req_form.findViewById(R.id.have_natCard))).isChecked()==true?"1":"2";
        traveller_job=((EditText)(other_req_form.findViewById(R.id.traveller_job))).getText().toString();
        flight_date=dateS;
        flight_time=timeS;
        country_code=phoneArray[0];
        phone=phoneArray[1];
        if(traveller_name.isEmpty() || traveller_passport.isEmpty() || traveller_destination.isEmpty() || traveller_job.isEmpty() || flight_date.isEmpty() || flight_time.isEmpty() || country_code.isEmpty() || phone.isEmpty() ){
            return false;
        }
        else if(req_type==1 & traveller_have_natCard.isEmpty()){
            return false;
        }
        else{
            return true;
        }

    }
    private synchronized void setPayment(){
        JSONObject json=new JSONObject();
        JSONObject subJSON=new JSONObject();
        try {
            subJSON.put("id",req_id);
            subJSON.put("req_type",2);
            subJSON.put("payment_method",paymentMethod);
            json.put("method","setPayment");
            json.put("data",subJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(okhttp.isNetworkConnected(getApplicationContext())) {
            okhttp.post(getString(R.string.url), json.toString(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("FAIL");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        try {
                            JSONObject resJSON = new JSONObject(responseStr);
                            JSONObject subresJSON = new JSONObject(resJSON.getString("data"));
                            if (Integer.parseInt(resJSON.get("error").toString()) == 1) {
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                                    }
                                });
                                onBackPressed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Response=" + responseStr);

                    } else {
                        System.out.println("Response=" + "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                    }
                }
            });
        }

    }
    private void sendReq(){
        //final double[] price = new double[2];
        JSONObject json=new JSONObject();
        JSONObject subJSON=new JSONObject();


        try {
            subJSON.put("device",1);
            subJSON.put("type",req_type);
            subJSON.put("login_type",1);
            subJSON.put("customers_id", shared.getString("user_id", ""));
            subJSON.put("name",traveller_name);
            subJSON.put("passport",traveller_passport);
            subJSON.put("flight_date",flight_date);
            subJSON.put("flight_time",flight_time);
            subJSON.put("country_code",country_code);
            subJSON.put("phone",phone);
            subJSON.put("destination",traveller_destination);
            subJSON.put("haveNCard",traveller_have_natCard);
            subJSON.put("job",traveller_job);
            //subJSON.put("price",pickedPrice);
            System.out.println("permit=" + traveller_visa_type);
            subJSON.put("permit", traveller_visa_type);
           // subJSON.put("companions",companionArray);
            json.put("method","otherRequest");
            json.put("data",subJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(okhttp.isNetworkConnected(getApplicationContext())) {
            okhttp.post(getString(R.string.url), json.toString(), new Callback() {

                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("FAIL");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String responseStr = response.body().string();
                        try {
                            JSONObject resJSON = new JSONObject(responseStr);
                            JSONObject subresJSON = new JSONObject(resJSON.getString("data"));
                            if (Integer.parseInt(resJSON.get("error").toString()) == 1) {
                                req_id = subresJSON.getString("id");
                                changeTotal(subresJSON.getString("price"));
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {

                                        Toast.makeText(getApplicationContext(), getString(R.string.try_later), Toast.LENGTH_SHORT).show();
                                    }
                                });
                                onBackPressed();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Response=" + responseStr);

                    } else {
                        System.out.println("Response=" + "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
                    }
                }
            });
        }

    }

    private void openNextActivity() throws IOException {

        if (lifo.size()==1) {
            findViewById(R.id.next_linear).setVisibility(View.GONE);
        }
        if(lifo.size()!=0){
            final View[] before = {findViewById((Integer) lifo.scndpeek())};
            View after = null;
            if(before[0].getId()==R.id.other_form_include){
                if(validateForm()) {

                    showLoading();
                    sendReq();
                    after = findViewById((Integer) lifo.pop());
                }
                else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.fill_error),Toast.LENGTH_LONG).show();
                    return;
                }
            }
            else if (before[0].getId()==R.id.passport_image_include && req_type!=2){
                if(passport_image!=null) {
                    showLoading();
                    uploadImage(101, passport_image.get(0), "passport_image");
                    after = findViewById((Integer) lifo.pop());
                }
                else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pickImage_error),Toast.LENGTH_LONG).show();
                    return;
                }
            }
            else if(before[0].getId()==R.id.visa_image_include && req_type!=2){
                if(visa_image!=null) {
                    showLoading();
                    uploadImage(102, visa_image.get(0), "visa_image");
                    after = findViewById((Integer) lifo.pop());
                }
                else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pickImage_error),Toast.LENGTH_LONG).show();
                    return;
                }
            }
            else if(before[0].getId()==R.id.payment_include ){
                showLoading();
                setPayment();
                if (paymentMethod == 2) {
                    Intent online_payment = new Intent(getApplicationContext(), online_payment.class);
                    shared.edit().putString("price", String.valueOf(pickedPrice)).apply();
                    startActivity(online_payment);
                }
                after =  findViewById((Integer) lifo.pop());
            }
            else if(before[0].getId()==R.id.success_process_include){

            }
            animateLayout(before[0],after);
        }
    }
    public void onBackPressed() {
        if(main){
            finish();
        }
        else{
            findViewById(R.id.next_linear).setVisibility(View.VISIBLE);
            View after = findViewById((Integer) lifo.scndpop());
            View before = findViewById((Integer) lifo.scndpeek());

            before.setVisibility(View.VISIBLE);
            after.setVisibility(View.GONE);


            before.setAlpha(1f);
        }
        if (lifo.scndsize()==1){
            main=true;
        }
    }
    public void animateLayout(final View before, final View after){
        Animator.AnimatorListener animatorListener=new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                System.out.println("SHOW "+after.getId());
                before.setVisibility(View.GONE);
                after.setVisibility(View.VISIBLE);
                //  finalAfter.animate().alpha(1f).setDuration(700);

                main = false;



            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
        before.animate().alpha(0f).setDuration(700).setListener(animatorListener);
        hideLoading();
    }
    public void showLoading(){
        final View main=findViewById(R.id.other_service_layout);
        final ProgressBar loading=findViewById(R.id.other_service_loading2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                main.setClickable(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    main.setForeground(getDrawable(R.color.loading_color));
                }

                loading.setVisibility(View.VISIBLE);
            }
        });
    }
    public void hideLoading(){
        final View main=findViewById(R.id.other_service_layout);
        final ProgressBar loading=findViewById(R.id.other_service_loading2);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                main.setClickable(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    main.setForeground(getDrawable(android.R.color.transparent));
                }
                loading.setVisibility(View.GONE);
            }
        });
    }

    private void changeTotal(String price) {
        ((TextView) payment_include.findViewById(R.id.total)).setText(price + getResources().getString(R.string.SDG));
    }
    private void changeMsg(int type) {
        TextView msg = success_inlcude.findViewById(R.id.msg);

        switch (type) {
            case 1: {
                msg.setText(getResources().getString(R.string.exit_visa_msg));
            }
            break;
            case 2: {
                msg.setText(getResources().getString(R.string.yellow_favor_msg));
            }
            break;
            case 3: {
                msg.setText(getResources().getString(R.string.nat_card_msg));
            }
            break;
        }
    }
}
